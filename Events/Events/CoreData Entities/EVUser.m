//
//  EVUser.m
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "EVUser.h"
#import "EVUser.h"
#import "Event.h"
#import "Like.h"
#import "Note.h"


@implementation EVUser

@dynamic avatar;
@dynamic birthDate;
@dynamic location;
@dynamic name;
@dynamic attendedEvents;
@dynamic friends;
@dynamic invitationReceivedFriends;
@dynamic invitationSentFriends;
@dynamic organizedEvents;
@dynamic notes;
@dynamic likes;

- (NSString *)basicInfoString
{
    return [NSString stringWithFormat:@"%@ from %@",self.name,self.location];
}

@end
