//
//  Like.m
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "Like.h"
#import "EVUser.h"
#import "Event.h"


@implementation Like

@dynamic date;
@dynamic owner;
@dynamic event;

@end
