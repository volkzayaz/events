//
//  EVUser.h
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EVUser, Event, Like, Note;

@interface EVUser : NSManagedObject

@property (nonatomic, retain) NSData * avatar;
@property (nonatomic, retain) NSDate * birthDate;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *attendedEvents;
@property (nonatomic, retain) NSSet *friends;
@property (nonatomic, retain) NSSet *invitationReceivedFriends;
@property (nonatomic, retain) NSSet *invitationSentFriends;
@property (nonatomic, retain) NSSet *organizedEvents;
@property (nonatomic, retain) NSSet *notes;
@property (nonatomic, retain) NSSet *likes;

- (NSString *)basicInfoString;

@end

@interface EVUser (CoreDataGeneratedAccessors)

- (void)addAttendedEventsObject:(Event *)value;
- (void)removeAttendedEventsObject:(Event *)value;
- (void)addAttendedEvents:(NSSet *)values;
- (void)removeAttendedEvents:(NSSet *)values;

- (void)addFriendsObject:(EVUser *)value;
- (void)removeFriendsObject:(EVUser *)value;
- (void)addFriends:(NSSet *)values;
- (void)removeFriends:(NSSet *)values;

- (void)addInvitationReceivedFriendsObject:(EVUser *)value;
- (void)removeInvitationReceivedFriendsObject:(EVUser *)value;
- (void)addInvitationReceivedFriends:(NSSet *)values;
- (void)removeInvitationReceivedFriends:(NSSet *)values;

- (void)addInvitationSentFriendsObject:(EVUser *)value;
- (void)removeInvitationSentFriendsObject:(EVUser *)value;
- (void)addInvitationSentFriends:(NSSet *)values;
- (void)removeInvitationSentFriends:(NSSet *)values;

- (void)addOrganizedEventsObject:(Event *)value;
- (void)removeOrganizedEventsObject:(Event *)value;
- (void)addOrganizedEvents:(NSSet *)values;
- (void)removeOrganizedEvents:(NSSet *)values;

- (void)addNotesObject:(Note *)value;
- (void)removeNotesObject:(Note *)value;
- (void)addNotes:(NSSet *)values;
- (void)removeNotes:(NSSet *)values;

- (void)addLikesObject:(Like *)value;
- (void)removeLikesObject:(Like *)value;
- (void)addLikes:(NSSet *)values;
- (void)removeLikes:(NSSet *)values;

@end
