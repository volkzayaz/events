//
//  Event.m
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "Event.h"
#import "EVUser.h"
#import "Note.h"


@implementation Event

@dynamic createdDate;
@dynamic endDate;
@dynamic eventDescription;
@dynamic image;
@dynamic name;
@dynamic notes;
@dynamic orginizer;
@dynamic participants;
@dynamic likes;

@end
