//
//  EVUser+CurrentUser.m
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "EVUser+CurrentUser.h"
#import <MagicalRecord/MagicalRecord.h>

static EVUser* _currentUser = nil;

NSString * const kObjectIdURIKey = @"objectIdURIKey";

@implementation EVUser (CurrentUser)

+ (instancetype)currentUser
{
    if(!_currentUser)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            NSURL* objectIdURI = [[NSUserDefaults standardUserDefaults] URLForKey:kObjectIdURIKey];
            if(objectIdURI)
            {
                NSManagedObjectID* objectId = [[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] managedObjectIDForURIRepresentation:objectIdURI];
                NSAssert(objectId != nil, @"MagicalRecord probably dropped DB so no objects correspond to given objectIdURI");
                _currentUser = (EVUser*)[[NSManagedObjectContext MR_defaultContext] objectWithID:objectId];
            }
        });
    }
    
    return _currentUser;
}

- (void)becomeCurrentUser
{
    NSManagedObjectID* objectId = self.objectID;
    [[NSUserDefaults standardUserDefaults] setURL:objectId.URIRepresentation
                                           forKey:kObjectIdURIKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    _currentUser = self;
}

- (UIImage*)avatarImage
{
    return [UIImage imageWithData:self.avatar];
}

@end
