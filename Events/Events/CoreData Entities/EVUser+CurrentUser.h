//
//  EVUser+CurrentUser.h
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "EVUser.h"
@import UIKit;

@interface EVUser (CurrentUser)

//as for now current User is returned as being created in MR_defaultContext
+ (instancetype) currentUser;
- (void) becomeCurrentUser;

- (UIImage*) avatarImage;

@end
