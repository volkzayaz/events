//
//  Like.h
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EVUser, Event;

@interface Like : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) EVUser *owner;
@property (nonatomic, retain) Event *event;

@end
