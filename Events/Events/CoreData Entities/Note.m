//
//  Note.m
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "Note.h"
#import "EVUser.h"
#import "Event.h"


@implementation Note

@dynamic text;
@dynamic date;
@dynamic author;
@dynamic event;

@end
