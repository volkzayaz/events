//
//  FriendRequestCell.m
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "FriendRequestCell.h"

@interface FriendRequestCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@property (nonatomic, strong) EVUser* user;
@property (nonatomic, copy) AddFriendCallback callback;

@end

@implementation FriendRequestCell

- (void)setUser:(EVUser *)user withCallback:(AddFriendCallback)callback
{
    self.user = user;
    self.callback = callback;
    
    self.avatarImageView.image = [UIImage imageWithData:user.avatar];
    self.infoLabel.text = user.basicInfoString;
}

- (IBAction)addFriend:(id)sender {
    !self.callback ?: self.callback();
}

@end
