//
//  EventCell.h
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "Like.h"

typedef NS_ENUM(NSInteger, EventAction){
    EventActionShowNotes,
    EventActionShowParticipants,
    EventActionShowLikes,
};

typedef void(^EventActionCallback)(EventAction action);

/**
 *  @param - like. This like was fetched on a private MOC. Do not assume that it was created on default MOC.
 */
typedef void(^EventLikeStatusChanged)(BOOL isLiked, Like* like);

@interface EventCell : UITableViewCell

@property (nonatomic, copy) EventLikeStatusChanged likeStatusCallback;
@property (nonatomic, copy) EventActionCallback eventActionCallback;

- (void) setEvent:(Event*)event;

@end
