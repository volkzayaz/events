//
//  FriendRequestCell.h
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EVUser.h"

typedef void(^AddFriendCallback)();

@interface FriendRequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *addFriendButton;

- (void) setUser:(EVUser*)user
    withCallback:(AddFriendCallback)callback;

@end
