//
//  EventCell.m
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "EventCell.h"
#import "EVUser+CurrentUser.h"
#import "NSDate+YearsOld.h"
#import "Note.h"

#import <MagicalRecord/MagicalRecord.h>

@interface EventCell()

@property (weak, nonatomic) IBOutlet UIImageView *eventImageView;
@property (weak, nonatomic) IBOutlet UILabel *interestingUserName;
@property (weak, nonatomic) IBOutlet UIButton *participantsCountButton;
@property (weak, nonatomic) IBOutlet UILabel *timeSinceEndLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstFiveNotesText;
@property (weak, nonatomic) IBOutlet UIButton *notesCountButton;
@property (weak, nonatomic) IBOutlet UIButton *likesCountButton;
@property (weak, nonatomic) IBOutlet UISwitch *likeSwitchView;

@property (nonatomic, strong) Like* likeOfCurrentUser;

@end

@implementation EventCell

- (void)setEvent:(Event *)event
{
    self.eventImageView.image = [UIImage imageWithData:event.image];
    
    if ([event.participants containsObject:[EVUser currentUser]])
    {
        self.interestingUserName.text = [EVUser currentUser].basicInfoString;
    }
    else
    {
        self.interestingUserName.text = event.orginizer.basicInfoString;
    }

    [self.participantsCountButton setTitle:[NSString stringWithFormat:@"Participants number is %lu",(unsigned long)event.participants.count]
                           forState:UIControlStateNormal];
    NSInteger hours = (long)event.endDate.hoursOld;
    if(hours > 0)
    {
        self.timeSinceEndLabel.text = [NSString stringWithFormat:@"Ended %li hours ago",(long)hours];
    }
    else
    {
        self.timeSinceEndLabel.text = [NSString stringWithFormat:@"Will start in %li hours",(long)-hours];
    }

    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"event",event];
    NSFetchRequest* firstFiveNotesRequest = [[NSFetchRequest alloc] initWithEntityName:@"Note"];
    
    firstFiveNotesRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES]];
    firstFiveNotesRequest.fetchLimit = 5;
    firstFiveNotesRequest.predicate = predicate;
    
    NSManagedObjectContext* localContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_rootSavingContext]];
    
    [localContext performBlock:^{
        ///retreiving first 5 notes
        NSArray* array = [localContext executeFetchRequest:firstFiveNotesRequest error:nil];
        NSMutableString* firstNotesText = [NSMutableString string];
        for(Note* note in array)
        {
            [firstNotesText appendFormat:@"%@/",note.text];
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            self.firstFiveNotesText.text = firstNotesText;
        }];
        
        ///counting total notes
        NSInteger notesCount = [Note MR_countOfEntitiesWithPredicate:predicate inContext:localContext];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
             [self.notesCountButton setTitle:[NSString stringWithFormat:@"Total notes %li",notesCount] forState:UIControlStateNormal];
        }];
        
       ///counting likes
        NSInteger likesCount = [Like MR_countOfEntitiesWithPredicate:predicate inContext:localContext];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.likesCountButton setTitle:[NSString stringWithFormat:@"Total likes %li",likesCount] forState:UIControlStateNormal];
        }];

        
        self.likeOfCurrentUser = [Like MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@)",@"owner",[EVUser currentUser],@"event",event] inContext:localContext];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            self.likeSwitchView.on = self.likeOfCurrentUser != nil;
        }];
    }];
}

- (IBAction)showNotes:(id)sender {
    !self.eventActionCallback ?: self.eventActionCallback(EventActionShowNotes);
}

- (IBAction)showParticipants:(id)sender {
    !self.eventActionCallback ?: self.eventActionCallback(EventActionShowParticipants);
}

- (IBAction)showLikes:(id)sender {
    !self.eventActionCallback ?: self.eventActionCallback(EventActionShowLikes);
}

- (IBAction)toggleLikeStatus:(id)sender {
    !self.likeStatusCallback ?: self.likeStatusCallback(self.likeSwitchView.isOn, self.likeOfCurrentUser);
}
@end
