//
//  NotesCell.m
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "NotesCell.h"
#import "EVUser.h"

@interface NotesCell ()

@property (weak, nonatomic) IBOutlet UILabel *noteText;
@property (weak, nonatomic) IBOutlet UIButton *authorButton;

@property (nonatomic, copy) NoteAuthorCliked callback;

@end

@implementation NotesCell

- (void)setNote:(Note *)note
       callback:(NoteAuthorCliked)callback
{
    self.noteText.text = [NSString stringWithFormat:@"%@. Posted on %@",note.text,note.date.description];
    [self.authorButton setTitle:[NSString stringWithFormat:@"By %@",note.author.name] forState:UIControlStateNormal];
    
    self.callback = callback;
}

- (IBAction)autorClicked:(id)sender {
    !self.callback ?: self.callback();
}

@end
