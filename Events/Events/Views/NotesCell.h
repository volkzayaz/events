//
//  NotesCell.h
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"

typedef void(^NoteAuthorCliked)();

@interface NotesCell : UITableViewCell

- (void) setNote:(Note*)note callback:(NoteAuthorCliked)callback;

@end
