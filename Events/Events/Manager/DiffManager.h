//
//  DiffManager.h
//  Events
//
//  Created by Vladislav Soroka on 7/7/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiffManager : NSObject

+ (instancetype) sharedInstance;

- (void) startListeningForChanges;

@end
