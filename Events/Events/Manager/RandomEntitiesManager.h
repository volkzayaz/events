//
//  RandomEntitiesManager.h
//  Events
//
//  Created by Vladislav Soroka on 7/7/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EVUser.h"
#import "Event.h"

@interface RandomEntitiesManager : NSObject

+ (instancetype) sharedInstance;

- (EVUser*) randomUserInContext:(NSManagedObjectContext*)context;
- (Event*) randomEventInContext:(NSManagedObjectContext*)context
                  withOrginizer:(EVUser*)orginizer;

@end
