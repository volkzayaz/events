//
//  UpdatesManager.m
//  Events
//
//  Created by Vladislav Soroka on 7/7/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "UpdatesManager.h"

#import <MagicalRecord/MagicalRecord.h>

#import "RandomEntitiesManager.h"

#import "Note.h"
#import "Like.h"

static UpdatesManager* _sharedInstance = nil;

@interface UpdatesManager ()

@property (nonatomic, strong) EVUser* focusedUser;
@property (nonatomic, strong) Event* focusedEvent;

@end

@implementation UpdatesManager

+ (instancetype)sharedInstance
{
    if(_sharedInstance)
        return _sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ///change send mode here
        _sharedInstance = [[UpdatesManager alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        
    }
    
    return self;
}

- (void)setFocusOnEvent:(Event *)event
{
    self.focusedUser = nil;
    self.focusedEvent = event;
}

- (void)setFocusOnUser:(EVUser *)user
{
    self.focusedEvent = nil;
    self.focusedUser = user;
}

#pragma mark - receiving diff simulation methods

- (void)startUpdateSimulation
{
    if(self.focusedUser)
    {
        [self createChangesFocusingOnUser:self.focusedUser];
    }
    else if(self.focusedEvent)
    {
        [self createChangesFocusingOnEvent:self.focusedEvent];
    }
    else
    {
        NSLog(@"Nothing ot focus at!");
    }
}

- (void) createChangesFocusingOnUser:(EVUser*)user
{
    ///we will pretend that we received data that tells us:
    ///1) New user(random) was created
    ///2) Newly created user and passed user are now friends
    ///3) Passed user orginized new event(Random)
    ///4) Newly created user attended this event
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        ///Phase 1
        EVUser* brandNewUser = [[RandomEntitiesManager sharedInstance] randomUserInContext:localContext];
        
        ///Phase 2
        
        ///this way we'll not make a roundtrip to store to retreive passed user
        NSManagedObjectID* objectID = user.objectID;
        EVUser* localUser = (EVUser*)[localContext objectWithID:objectID];
        [brandNewUser addFriendsObject:localUser];
        
        ///Phase 3
        Event* newEvent = [[RandomEntitiesManager sharedInstance] randomEventInContext:localContext
                                                                         withOrginizer:localUser];
        ///Phase 4
        [newEvent addParticipantsObject:brandNewUser];
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        if(!error)
        {
            NSLog(@"Succesffuly imported data with focus on user %@",user.name);
        }
    }];
}

- (void) createChangesFocusingOnEvent:(Event*)event
{
    ///we will pretend that we received data that tells us:
    ///1) New user(random) was created and become participant of passed event
    ///2) Newly created user added note to passed event
    ///3) Newly created user liked passed event
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        Event* localEvent = (Event*)[localContext objectWithID:event.objectID];
        
        ///Phase 1
        EVUser* brandNewUser = [[RandomEntitiesManager sharedInstance] randomUserInContext:localContext];
        [brandNewUser addAttendedEventsObject:localEvent];
        
        ///Phase 2
        Note* newNote = [Note MR_createEntityInContext:localContext];
        newNote.author = brandNewUser;
        newNote.event = localEvent;
        newNote.date = [NSDate date];
        newNote.text = [NSString stringWithFormat:@"Yo note. Here's a random number %i",arc4random_uniform(10000)];
        
        ///Phase 3
        Like* like = [Like MR_createEntityInContext:localContext];
        like.owner = brandNewUser;
        like.event = localEvent;
        like.date = [NSDate date];
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        if(!error)
        {
            NSLog(@"Succesffuly imported data with focus on event %@",event.name);
        }
    }];
}

@end
