//
//  RandomEntitiesManager.m
//  Events
//
//  Created by Vladislav Soroka on 7/7/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "RandomEntitiesManager.h"
#import <MagicalRecord/MagicalRecord.h>


static RandomEntitiesManager* _sharedInstance = nil;

@interface RandomEntitiesManager()

@property (nonatomic, strong) NSArray* userNames;
@property (nonatomic, strong) NSArray* userLocations;
@property (nonatomic, strong) NSArray* userAvatarImages;

@property (nonatomic, strong) NSArray* eventNames;
@property (nonatomic, strong) NSArray* eventDescriptions;
@property (nonatomic, strong) NSArray* eventImages;

@end

@implementation RandomEntitiesManager

+ (instancetype)sharedInstance
{
    if(_sharedInstance)
        return _sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ///change send mode here
        _sharedInstance = [[RandomEntitiesManager alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        self.userNames = @[@"Josh", @"Pete", @"Jack", @"Lindsey", @"Mellany", @"Richard", @"Josef", @"Miranda", @"Bill", @"Carly", @"Samantha", @"Harry", @"Ron", @"Jon Snow"];
        self.userLocations = @[@"London", @"Paris" , @"Madrid", @"Berlin", @"Hogwarts", @"Narnia", @"Neverland", @"Ukraine"];
        self.userAvatarImages = @[@"Boo.jpg",@"girl.jpeg",@"Boy.jpeg",@"man.jpeg",@"leGirl.jpeg",@"dude.jpeg"];
        
        
        self.eventNames = @[@"Party", @"Rollercoast", @"Chess tournament", @"Sport Match", @"Cinema visiting", @"Granny funeral", @"Apple store line", @"Stripclub"];
        self.eventDescriptions = @[@"It's gonna be sick! Everybody gather your friends and let's go!",
                              @"It's a sad day for us, so let's give the last honor at this hour.",
                              @"Don't miss this action! You're a looser if you do. Meeting at 6.30 on Groove street. Meet all your street cats at one place!",
                              @"Sure thing you'd like to go. I'll stab you if you miss this event! Just joking. But seriously, be in place by 12.00 because we're planning on hitting the road and have lot's of fun!",
                              @"Not an easy day, but we gotta do that. Show some respect and be at 6-th avenue tomorrow at 4.30PM. Janneth will be very pleased if you come.",
                              @"Are you ready to become naughty? Can you feel the pleasure flowing through your body to the tips of your toes? That's only a tiny part of the emotions you'll get if you come here at 1AM tomorrow on Back street 7. Don't miss it or you'll regret this!"];
        
        self.eventImages = @[@"funeral.jpeg",@"stripclub.jpeg",@"mountains.jpeg",@"ranch.jpg",@"party.jpeg",@"NY.jpeg",@"nba.jpeg"];
    }
    
    return self;
}

- (EVUser *)randomUserInContext:(NSManagedObjectContext *)localContext
{
    EVUser* newUser = [EVUser MR_createEntityInContext:localContext];
    
    u_int32_t nameIndex = arc4random_uniform((u_int32_t)self.userNames.count);
    u_int32_t bithdateNumber = arc4random_uniform([[NSDate date] timeIntervalSince1970]);
    u_int32_t locationIndex = arc4random_uniform((u_int32_t)self.userLocations.count);
    u_int32_t avatarIndex = arc4random_uniform((u_int32_t)self.userAvatarImages.count);
    
    newUser.name = self.userNames[nameIndex];
    newUser.birthDate = [NSDate dateWithTimeIntervalSince1970:bithdateNumber];
    newUser.location = self.userLocations[locationIndex];
    UIImage* image = [UIImage imageNamed:self.userAvatarImages[avatarIndex]];
    newUser.avatar = UIImageJPEGRepresentation(image, 0.5);
    
    return newUser;
}

- (Event *)randomEventInContext:(NSManagedObjectContext *)localContext withOrginizer:(EVUser *)orginizer
{
    Event* newEvent = [Event MR_createEntityInContext:localContext];
    
    ///each random event will have createDate +/- 3 days. End of the event will be scheduled in a week.
    NSTimeInterval threeDaysInSeconds = 259200;
    NSTimeInterval secondsInWeek = 604800;
    
    u_int32_t nameIndex = arc4random_uniform((u_int32_t)self.eventNames.count);
    u_int32_t startDateIndex = arc4random_uniform(threeDaysInSeconds * 2);
    u_int32_t descriptionIndex = arc4random_uniform((u_int32_t)self.eventDescriptions.count);
    u_int32_t imageIndex = arc4random_uniform((u_int32_t)self.eventImages.count);
    
    newEvent.name = self.eventNames[nameIndex];
    NSTimeInterval secondsDeviationFromNow = ((NSTimeInterval)startDateIndex - threeDaysInSeconds);
    newEvent.createdDate = [NSDate dateWithTimeIntervalSinceNow:secondsDeviationFromNow];
    newEvent.endDate = [NSDate dateWithTimeIntervalSinceNow:secondsDeviationFromNow + secondsInWeek];
    newEvent.eventDescription = self.eventDescriptions[descriptionIndex];
    UIImage* image = [UIImage imageNamed:self.eventImages[imageIndex]];
    newEvent.image = UIImageJPEGRepresentation(image, 0.5);
    
    NSManagedObjectID* objId = orginizer.objectID;
    EVUser* localCurrentUser = (EVUser*)[localContext objectWithID:objId];
    newEvent.orginizer = localCurrentUser;
    [newEvent addParticipantsObject:localCurrentUser];
    
    return newEvent;
}

@end
