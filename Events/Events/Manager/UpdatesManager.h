//
//  UpdatesManager.h
//  Events
//
//  Created by Vladislav Soroka on 7/7/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Event.h"
#import "EVUser.h"

@interface UpdatesManager : NSObject

+ (instancetype) sharedInstance;

///setting focus will make sure that simulated diff. that will be received, will be related to passed object.
///setting focus on event automatically removes focus from passed user and vice versa
- (void) setFocusOnEvent:(Event*)event;
- (void) setFocusOnUser:(EVUser*)user;

- (void) startUpdateSimulation;

@end
