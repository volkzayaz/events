//
//  DiffManager.m
//  Events
//
//  Created by Vladislav Soroka on 7/7/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "DiffManager.h"

#import <MagicalRecord/MagicalRecord.h>
@import CoreData;

static DiffManager* _sharedInstance = nil;

#define KILOBYTES_IN_PACKAGE 50
#define SECONDS_BETWEEN_UPDATES 5


typedef NS_ENUM(NSInteger, SendMode)
{
    SendModeBytes,
    SendModeTime
};

@interface DiffManager ()

@property (nonatomic, strong) NSMutableDictionary* deleteChangesDictionary;
@property (nonatomic, strong) NSMutableDictionary* updateChangesDictionary;
@property (nonatomic, strong) NSMutableDictionary* insertChangesDictionary;

@property (nonatomic, assign) SendMode mode;

@property (nonatomic, strong) NSOperationQueue* privateQueue;

@end

@implementation DiffManager

+ (instancetype)sharedInstance
{
    if(_sharedInstance)
        return _sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ///change send mode here
        _sharedInstance = [[DiffManager alloc] initWithMode:SendModeBytes];
    });
    
    return _sharedInstance;
}

- (instancetype) initWithMode:(SendMode)mode
{
    self = [super init];
    
    if(self)
    {
        self.updateChangesDictionary = [NSMutableDictionary dictionary];
        self.insertChangesDictionary = [NSMutableDictionary dictionary];
        self.deleteChangesDictionary = [NSMutableDictionary dictionary];
        
        self.mode = mode;
        
        self.privateQueue = [[NSOperationQueue alloc] init];
        self.privateQueue.maxConcurrentOperationCount = 1;
    }
    
    return self;
}

- (void) startListeningForChanges
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(storeChangesFromNotification:)
                                                 name:NSManagedObjectContextWillSaveNotification
                                               object:[NSManagedObjectContext MR_rootSavingContext]];
    if(self.mode == SendModeTime)
    {
        [NSTimer scheduledTimerWithTimeInterval:SECONDS_BETWEEN_UPDATES
                                         target:self
                                       selector:@selector(enqueueServerSendOperation)
                                       userInfo:nil
                                        repeats:YES];
    }
}

- (void) storeChangesFromNotification:(NSNotification*) note
{
    __weak typeof(self) weakSelf = self;
    [self.privateQueue addOperationWithBlock:^{
        NSManagedObjectContext *context = note.object;
        NSSet *insertedObjects = [context insertedObjects];
        NSSet *deletedObjects = [context deletedObjects];
        NSSet *updatedObjects = [context updatedObjects];
        
        for(NSManagedObject* object in updatedObjects)
        {
            ///here some merging policy might be implemented
            ///our policy will be simply upsert value for given key
            NSURL* objectId = [object objectID].URIRepresentation;
            weakSelf.updateChangesDictionary[objectId] = [weakSelf mapChanges:[object changedValues]];
        }
        
        for(NSManagedObject* object in insertedObjects)
        {
            NSURL* objectId = [object objectID].URIRepresentation;
            weakSelf.insertChangesDictionary[objectId] = [weakSelf mapChanges:[object changedValues]];
        }
        
        for(NSManagedObject* object in deletedObjects)
        {
            NSURL* objectId = [object objectID].URIRepresentation;
            weakSelf.deleteChangesDictionary[objectId] = [weakSelf mapChanges:[object changedValues]];
        }
        
        if(weakSelf.mode == SendModeBytes)
        {
            if([weakSelf isByteLimitExceeded])
            {
                [weakSelf enqueueServerSendOperation];
            }
        }
    }];
}

- (void) enqueueServerSendOperation
{
    if(self.insertChangesDictionary.allKeys.count == 0&&
       self.updateChangesDictionary.allKeys.count == 0&&
       self.deleteChangesDictionary.allKeys.count == 0)
    {
        return;
    }
    NSArray* rootArray = @[self.insertChangesDictionary,self.deleteChangesDictionary,self.updateChangesDictionary];
    NSData* data = [NSKeyedArchiver archivedDataWithRootObject:rootArray];
    
    __weak typeof(self) weakSelf = self;
    
    [self.privateQueue addOperationWithBlock:^{
        
        NSLog(@"Sending to server %lu bytes",data.length);
        
        [weakSelf.insertChangesDictionary removeAllObjects];
        [weakSelf.updateChangesDictionary removeAllObjects];
        [weakSelf.deleteChangesDictionary removeAllObjects];
    }];
}

#pragma mark - auxilary methods

- (BOOL) isByteLimitExceeded
{
    NSArray* rootArray = @[self.insertChangesDictionary,self.deleteChangesDictionary,self.updateChangesDictionary];
    NSData* data = [NSKeyedArchiver archivedDataWithRootObject:rootArray];
    return data.length > 1024 * KILOBYTES_IN_PACKAGE;
}

- (NSDictionary*) mapChanges:(NSDictionary*)changes
{
    NSMutableDictionary* results = [NSMutableDictionary dictionary];
    for(NSString* key in [changes allKeys])
    {
        id value = [changes valueForKey:key];
        if([value isKindOfClass:[NSManagedObject class]])
        {
            [results setObject:[value objectID].URIRepresentation forKey:key];
        }
        else if ([value isKindOfClass:[NSSet class]])
        {
            NSSet* objectIds = [value valueForKeyPath:@"objectID.URIRepresentation"];
            [results setObject:objectIds forKey:key];
        }
        else
        {
            [results setObject:value forKey:key];
        }
    }
    
    return results.copy;
}

@end
