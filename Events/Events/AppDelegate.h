//
//  AppDelegate.h
//  Events
//
//  Created by Vladislav Soroka on 7/1/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

