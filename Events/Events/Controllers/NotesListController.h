//
//  NotesListController.h
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListProviderController.h"

@interface NotesListController : ListProviderController <ListProviderDelegate>

@end
