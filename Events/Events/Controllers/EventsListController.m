//
//  EventsListController.m
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

////controllers
#import "EventsListController.h"
#import "EventPageController.h"
#import "LikesController.h"
#import "NotesListController.h"
#import "UserListController.h"

///model
#import "Event.h"
#import "EVUser+CurrentUser.h"
#import "Like.h"
#import "Note.h"

///categories
#import "UIViewController+RestorationID.h"

///other
#import <MagicalRecord/MagicalRecord.h>

#import "EventCell.h"

#import "RandomEntitiesManager.h"

@interface EventsListController ()



@property (nonatomic, assign) BOOL shouldIncludeCreationTools;

@end

@implementation EventsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.shouldIncludeCreationTools)
    {
        UIBarButtonItem* addRandomEventItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                            target:self
                                                                                            action:@selector(addRandomEvent:)];
        self.navigationItem.rightBarButtonItem = addRandomEventItem;
    }
   
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200.;
}

- (void)turnTimelineModeOn
{
    if(![EVUser currentUser])
        return;

    self.shouldIncludeCreationTools = YES;
    
    NSSet* meAndMyFriends = [[EVUser currentUser].friends setByAddingObject:[EVUser currentUser]];

    NSPredicate* orginizedEvents = [NSPredicate predicateWithFormat:@"%@ CONTAINS %K",meAndMyFriends,@"orginizer"];
    NSPredicate* attendedEventsPredicate = [NSPredicate predicateWithFormat:@"ANY %K IN %@",@"participants",meAndMyFriends];
    self.fetchedResultsController = [Event MR_fetchAllGroupedBy:nil
                                                            withPredicate:[NSCompoundPredicate orPredicateWithSubpredicates:@[orginizedEvents,attendedEventsPredicate]]
                                                                 sortedBy:@"createdDate"
                                                                ascending:YES];
    
}

#pragma mark - listProviderDelegate

- (NSString *)tableViewCellReuseID
{
    return NSStringFromClass([EventCell class]);
}

- (void)configureCell:(EventCell *)cell withManagedObject:(Event *)object
{
    Event* event = object;
    
    [cell setEvent:event];
    
    __weak typeof(self) weakSelf = self;
    cell.eventActionCallback = ^(EventAction action){
        switch (action) {
            case EventActionShowLikes:
            {
                LikesController* controller = [weakSelf.storyboard instantiateViewControllerWithIdentifier:[LikesController restorationId]];
                
                NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"Like"];
                
                request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES]];
                request.predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"event",object];
                [request setRelationshipKeyPathsForPrefetching:@[@"owner"]];
                
                controller.fetchedResultsController = [Like MR_fetchController:request
                                                                      delegate:nil
                                                                  useFileCache:NO
                                                                     groupedBy:nil
                                                                     inContext:[NSManagedObjectContext MR_defaultContext]];
                [Like MR_performFetch:controller.fetchedResultsController];
                
                [weakSelf.navigationController pushViewController:controller animated:YES];
                break;
            }
            case EventActionShowNotes:
            {
                NotesListController* controller = [weakSelf.storyboard instantiateViewControllerWithIdentifier:[NotesListController restorationId]];
                
                NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"Note"];
                
                request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES]];
                request.predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"event",object];
                [request setRelationshipKeyPathsForPrefetching:@[@"author"]];
                
                controller.fetchedResultsController = [Note MR_fetchController:request
                                                                      delegate:nil
                                                                  useFileCache:NO
                                                                     groupedBy:nil
                                                                     inContext:[NSManagedObjectContext MR_defaultContext]];
                [Note MR_performFetch:controller.fetchedResultsController];
                
                [weakSelf.navigationController pushViewController:controller animated:YES];
                break;
            }
            case EventActionShowParticipants:
            {
                UserListController* controller = [weakSelf.storyboard instantiateViewControllerWithIdentifier:[UserListController restorationId]];
                
                [controller setFetchedResultsController:[EVUser MR_fetchAllGroupedBy:nil
                                                                       withPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS %@",@"attendedEvents",object]
                                                                            sortedBy:@"name"
                                                                           ascending:YES]];
                
                [weakSelf.navigationController pushViewController:controller animated:YES];
                break;
            }
            default:
                break;
        }
    };
    
    cell.likeStatusCallback = ^(BOOL isLiked, Like* like){
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            if(isLiked)//user has just liked this post
            {
                Like* newLike = [Like MR_createEntityInContext:localContext];
                
                newLike.date = [NSDate date];
                newLike.owner = (EVUser*)[localContext objectWithID:[EVUser currentUser].objectID];
                newLike.event = (Event*)[localContext objectWithID:event.objectID];
            }
            else//user has just unliked this post
            {
                Like* localLike = (Like*)[localContext objectWithID:like.objectID];
                [localLike MR_deleteEntityInContext:localContext];
            }
        }];
    };
}

- (void)didSelectManagedObject:(NSManagedObject *)object
{
    Event* event = (Event*)object;
    
    EventPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[EventPageController restorationId]];
    controller.eventToPresent = event;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - auxilary methods

- (void) addRandomEvent:(id) sender
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        [[RandomEntitiesManager sharedInstance] randomEventInContext:localContext withOrginizer:[EVUser currentUser]];
    }];
}

@end
