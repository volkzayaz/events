//
//  UserPageController.m
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

///controllers
#import "UserPageController.h"
#import "EventsListController.h"
#import "UserListController.h"
#import "NotesListController.h"
#import "ReceivedFriendshipController.h"

///categories
#import "EVUser+CurrentUser.h"
#import "NSDate+YearsOld.h"
#import "UIViewController+RestorationID.h"
#import "UIViewController+ErrorMessages.h"

///model
#import "Note.h"
#import "Event.h"

///others
#import <MagicalRecord/MagicalRecord.h>
#import "UpdatesManager.h"

@interface UserPageController ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameAndLocationLabel;
@property (weak, nonatomic) IBOutlet UIButton *friendsCountButton;
@property (weak, nonatomic) IBOutlet UIButton *friendRequestButton;
@property (weak, nonatomic) IBOutlet UIButton *friendRequestSentButton;

@property (weak, nonatomic) IBOutlet UIButton *ownNotesButton;
@property (weak, nonatomic) IBOutlet UIButton *receivedNotesButton;

@property (weak, nonatomic) IBOutlet UIButton *createdEventsButton;
@property (weak, nonatomic) IBOutlet UIButton *attendedEvents;

@property (nonatomic, strong) UIBarButtonItem* friendRequestItem;



@end

@implementation UserPageController

- (void)loadView
{
    [super loadView];

    self.friendRequestItem =    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                          target:self
                                                                          action:@selector(sendFriendRequest:)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NSManagedObjectContextObjectsDidChangeNotification
                                                      object:[NSManagedObjectContext MR_defaultContext]
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      ///Magical Record has already taken care of merging stuff
                                                    
                                                    ///while it might be a good idea to parse changes from notification and update only relevant fields,
                                                    ///here we'll just populate the whole view with the most recent data from the context
                                                      [self refreshView];
    }];
    
    [self refreshView];
    [[UpdatesManager sharedInstance] setFocusOnUser:_userToDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSManagedObjectContextObjectsDidChangeNotification
                                                  object:[NSManagedObjectContext MR_defaultContext]];
    
}

- (void)setUserToDisplay:(EVUser *)userToDisplay
{
    _userToDisplay = userToDisplay;
    
    [self refreshView];
}

#pragma mark - auxilary methods

- (void) refreshView{
    EVUser* user = self.userToDisplay;
    
    if([user isEqual:EVUser.currentUser])
    {
        self.title = @"It's you";
    }
    
    self.avatarImageView.image = [UIImage imageWithData:user.avatar];
    self.nameAndLocationLabel.text = [NSString stringWithFormat:@"%@ from %@. %li years old",self.userToDisplay.name, self.userToDisplay.location,(long)[user.birthDate yearsOld]];

    [self.createdEventsButton setTitle:[NSString stringWithFormat:@"Organized %lu events",(unsigned long)user.organizedEvents.count] forState:UIControlStateNormal];
    [self.attendedEvents setTitle:[NSString stringWithFormat:@"Attended %lu events",(unsigned long)user.attendedEvents.count] forState:UIControlStateNormal];
    
    ///showing/hiding friend request button
    if([EVUser.currentUser.invitationSentFriends containsObject:user] ||
       [EVUser.currentUser.friends containsObject:user] ||
       user == EVUser.currentUser)
    {
        [self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
    else
    {
        [self.navigationItem setRightBarButtonItem:self.friendRequestItem animated:YES];
    }
    
    [self.friendsCountButton setTitle:[NSString stringWithFormat:@"%lu friends", (unsigned long)user.friends.count]
                             forState:UIControlStateNormal];
    [self.friendRequestButton setTitle:[NSString stringWithFormat:@"%lu requests received", (unsigned long)user.invitationReceivedFriends.count]
                             forState:UIControlStateNormal];
    [self.friendRequestSentButton setTitle:[NSString stringWithFormat:@"%lu requests sent", (unsigned long)user.invitationSentFriends.count]
                                  forState:UIControlStateNormal];
    
    NSManagedObjectContext* privateContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_rootSavingContext]];
    
    [privateContext performBlock:^{
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"author",self.userToDisplay];
        NSInteger ownNotesCount = [Note MR_countOfEntitiesWithPredicate:predicate inContext:privateContext];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.ownNotesButton setTitle:[NSString stringWithFormat:@"Notes written %lu",ownNotesCount]
                                 forState:UIControlStateNormal];
        }];
    }];
    
    [privateContext performBlock:^{
        NSPredicate* authorPredicate = [NSPredicate predicateWithFormat:@"%K != %@",@"author",self.userToDisplay];
        NSPredicate* eventsPredicate = [NSPredicate predicateWithFormat:@"%K IN %@",@"event",self.userToDisplay.attendedEvents];
        NSInteger otherNotesCount = [Note MR_countOfEntitiesWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[authorPredicate,eventsPredicate]] inContext:privateContext];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.receivedNotesButton setTitle:[NSString stringWithFormat:@"Received %lu notes",otherNotesCount]
                                      forState:UIControlStateNormal];
        }];
    }];
}

#pragma mark - target/action calls

- (IBAction)showOrginizedEvents:(id)sender {
    EventsListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[EventsListController restorationId]];
    
    controller.fetchedResultsController = [Event MR_fetchAllGroupedBy:nil
                                                        withPredicate:[NSPredicate predicateWithFormat:@"%K == %@",@"orginizer",self.userToDisplay]
                                                             sortedBy:@"name"
                                                            ascending:YES];
    
    [self.navigationController pushViewController:controller animated:YES];

}

- (IBAction)showAttendedEvents:(id)sender {
    EventsListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[EventsListController restorationId]];
    
    controller.fetchedResultsController = [Event MR_fetchAllGroupedBy:nil
                                                        withPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS %@",@"participants",self.userToDisplay]
                                                             sortedBy:@"name"
                                                            ascending:YES];
    
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)showFriendRequestsReceived:(id)sender {
    ReceivedFriendshipController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[ReceivedFriendshipController restorationId]];
    
    [controller setUserToDisplay:self.userToDisplay];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)showFriendRequestsSent:(id)sender {
    UserListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[UserListController restorationId]];
    
    controller.fetchedResultsController = [EVUser MR_fetchAllGroupedBy:nil
                                                         withPredicate:[NSPredicate predicateWithFormat:@"%@ IN %K",self.userToDisplay, @"invitationReceivedFriends"]
                                                              sortedBy:@"name"
                                                             ascending:YES];
    
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)showFriends:(id)sender {
    UserListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[UserListController restorationId]];
    
    controller.fetchedResultsController = [EVUser MR_fetchAllGroupedBy:nil
                                                         withPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS %@",@"friends",self.userToDisplay]
                                                              sortedBy:@"name"
                                                             ascending:YES];
    
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)showOwnNotes:(id)sender {
    NotesListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[NotesListController restorationId]];
    
    controller.fetchedResultsController =  [Note MR_fetchAllGroupedBy:nil
                                                        withPredicate:[NSPredicate predicateWithFormat:@"%K == %@",@"author",self.userToDisplay]
                                                             sortedBy:@"date"
                                                            ascending:YES];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)showReceivedNotes:(id)sender {
    NotesListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[NotesListController restorationId]];

    NSPredicate* authorPredicate = [NSPredicate predicateWithFormat:@"%K != %@",@"author",self.userToDisplay];
    NSPredicate* eventsPredicate = [NSPredicate predicateWithFormat:@"%K IN %@",@"event",self.userToDisplay.attendedEvents];
    
    [controller setFetchedResultsController:[Note MR_fetchAllGroupedBy:nil
                                                         withPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[eventsPredicate,authorPredicate]]
                                                              sortedBy:@"date"
                                                             ascending:YES]];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) sendFriendRequest:(id)sender
{
    [self.userToDisplay addInvitationReceivedFriendsObject:EVUser.currentUser];
    
    [self showInfoMessage:@"Friend request has been sent" withTitle:@"Success"];
}

@end
