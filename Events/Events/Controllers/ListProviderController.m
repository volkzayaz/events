//
//  ListProviderController.m
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "ListProviderController.h"
#import <MagicalRecord/MagicalRecord.h>

@interface ListProviderController () <NSFetchedResultsControllerDelegate, ListProviderDelegate>

@end

@implementation ListProviderController

- (void)setFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController
{
    _fetchedResultsController = fetchedResultsController;
    
    _fetchedResultsController.delegate = self;
}

- (void) loadView
{
    [super loadView];
    
    self.listProviderDelegate = self;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString* reuseID = [self.listProviderDelegate tableViewCellReuseID];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID forIndexPath:indexPath];
    
    [self.listProviderDelegate configureCell:cell withManagedObject:object];
    return cell;
}

#pragma mark - Fetched results controller

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self.listProviderDelegate configureCell:[tableView cellForRowAtIndexPath:indexPath] withManagedObject:anObject];
            
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if([self.listProviderDelegate respondsToSelector:@selector(didSelectManagedObject:)])
    {
        [self.listProviderDelegate didSelectManagedObject:object];
    }
}


#pragma mark - list Provider Delegate

- (NSString*) tableViewCellReuseID
{
    NSAssert(NO, @"Method %@ was not implemented in %@",NSStringFromSelector(_cmd),NSStringFromClass([self class]));
    return nil;
}

- (void) configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    NSAssert(NO, @"Method %@ was not implemented in %@", NSStringFromSelector(_cmd), NSStringFromClass([self class]));
}

@end
