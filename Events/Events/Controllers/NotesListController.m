//
//  NotesListController.m
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

///controllers
#import "NotesListController.h"
#import "UserPageController.h"

///categories
#import "UIViewController+RestorationID.h"

///model
#import "Note.h"

///views
#import "NotesCell.h"

@interface NotesListController ()

@end

@implementation NotesListController

- (void)loadView
{
    [super loadView];
    
    self.tableView.rowHeight = UITableViewRowAnimationAutomatic;
    self.tableView.estimatedRowHeight = 50;
    
    self.title = @"Notes";
}

#pragma mark - Table view data source

- (NSString*) tableViewCellReuseID
{
    return NSStringFromClass([NotesCell class]);
}

- (void) configureCell:(NotesCell *)cell withManagedObject:(Note *)object
{
    Note* note = object;
    
    __weak typeof(self) weakSelf = self;
    [cell setNote:note callback:^{
        [weakSelf showNoteAuthor:note];
    }];
}


- (void) showNoteAuthor:(Note*) note
{
    UserPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[UserPageController restorationId]];
    
    [controller setUserToDisplay:note.author];
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end
