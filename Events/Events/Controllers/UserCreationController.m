//
//  MasterViewController.m
//  Events
//
//  Created by Vladislav Soroka on 7/1/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "UserCreationController.h"
#import "EventsListController.h"
#import "UIViewController+ErrorMessages.h"

#import "EVUser.h"
#import "EVUser+CurrentUser.h"

#import <MagicalRecord/MagicalRecord.h>
#import "RandomEntitiesManager.h"
#import "UpdatesManager.h"

@interface UserCreationController () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;


@end

@implementation UserCreationController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {

    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addRandomUser:)];
    self.navigationItem.rightBarButtonItem = addButton;
}


- (void)addRandomUser:(id)sender {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        [[RandomEntitiesManager sharedInstance] randomUserInContext:localContext];
    }];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    EVUser *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = user.basicInfoString;
    cell.detailTextLabel.text = [user.birthDate description];
    cell.imageView.image = [UIImage imageWithData:user.avatar];
    
    cell.accessoryType = [user.objectID isEqual:[EVUser currentUser].objectID] ? UITableViewCellAccessoryCheckmark :  UITableViewCellAccessoryNone;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    
    NSFetchedResultsController *aFetchedResultsController =
    [EVUser MR_fetchAllSortedBy:@"birthDate" ascending:YES withPredicate:nil groupBy:nil delegate:self];
    
    
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showInfoMessage:@"You are about to change current user. All navigation controllers will be sent popToRootViewController message. Proceed?" withTitle:@"Warning" callback:^{
        
        NSMutableArray* indexPathsToReload = [NSMutableArray arrayWithObject:indexPath];
        
        if([EVUser currentUser])
        {
            NSInteger previousCurrentUserIndex = [self.fetchedResultsController.fetchedObjects indexOfObject:[EVUser currentUser]];
            if(previousCurrentUserIndex != NSNotFound)
            {
                [indexPathsToReload addObject:[NSIndexPath indexPathForItem:previousCurrentUserIndex inSection:0]];
            }
        }
        
        EVUser* user = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        [user becomeCurrentUser];
        [[UpdatesManager sharedInstance] setFocusOnUser:user];
        
        [tableView reloadRowsAtIndexPaths:indexPathsToReload
                         withRowAnimation:UITableViewRowAnimationAutomatic];
        
        for(int i = 1;i < self.tabBarController.viewControllers.count;++i)
        {
            UINavigationController* controller = self.tabBarController.viewControllers[i];
            [controller popToRootViewControllerAnimated:NO];
            
            if([controller.viewControllers.firstObject isKindOfClass:[EventsListController class]])
            {
                EventsListController* eventsTimeline = controller.viewControllers.firstObject;
                
                ///this will work as refresh datasource
                [eventsTimeline turnTimelineModeOn];
                [eventsTimeline.tableView reloadData];
            }
        }
    }];
    
}

/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */

@end
