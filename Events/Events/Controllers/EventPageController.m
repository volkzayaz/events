//
//  EventPageController.m
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

///controllers
#import "EventPageController.h"
#import "UserPageController.h"
#import "UserListController.h"
#import "NotesListController.h"
#import "LikesController.h"

///model
#import "EVUser+CurrentUser.h"
#import "Note.h"
#import "Like.h"

///categories
#import "UIViewController+RestorationID.h"

///other
#import <MagicalRecord/MagicalRecord.h>
#import "UpdatesManager.h"

@interface EventPageController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *basicInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *authorButton;
@property (weak, nonatomic) IBOutlet UIButton *attendantsButton;
@property (weak, nonatomic) IBOutlet UIButton *notesButton;
@property (weak, nonatomic) IBOutlet UIButton *likesButton;

@property (weak, nonatomic) UIBarButtonItem* attendItem;
@property (strong, nonatomic) UIBarButtonItem* composeNoteItem;

@property (weak, nonatomic) IBOutlet UISwitch *likeSwitch;
@property (weak, nonatomic) IBOutlet UILabel *startsOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *endsOnLabel;

@property (nonatomic, strong) Like* likeOfCurrentUser;

@end

@implementation EventPageController

- (void)loadView
{
    [super loadView];
    
    UIBarButtonItem* attendEventItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(switchAttendaceStatus)];
    UIBarButtonItem* composeNoteItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentNoteCompositionUI)];
    
    self.navigationItem.rightBarButtonItems = @[composeNoteItem, attendEventItem,];
    
    self.attendItem = attendEventItem;
    self.composeNoteItem = composeNoteItem;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NSManagedObjectContextObjectsDidChangeNotification
                                                      object:[NSManagedObjectContext MR_defaultContext]
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      ///Magical Record has already taken care of merging stuff
                                                      [self refreshView];
                                                  }];
    [[UpdatesManager sharedInstance] setFocusOnEvent:self.eventToPresent];
    
    [self refreshView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSManagedObjectContextObjectsDidChangeNotification
                                                  object:[NSManagedObjectContext MR_defaultContext]];
}

- (void) refreshView
{
    Event* event = self.eventToPresent;
    
    self.navigationItem.title = event.name;
    self.basicInfoLabel.text = event.eventDescription;
    self.imageView.image = [UIImage imageWithData:event.image];
    [self.authorButton setTitle:[NSString stringWithFormat:@"Orginized by %@",event.orginizer.name] forState:UIControlStateNormal];
    [self.attendantsButton setTitle:[NSString stringWithFormat:@"Participants number is %lu",(unsigned long)event.participants.count]
                           forState:UIControlStateNormal];
    BOOL isAttendingEvent = [[EVUser currentUser].attendedEvents containsObject:event];
    if(isAttendingEvent)
    {
        self.attendItem.title = @"Leave";
        [self.navigationItem setRightBarButtonItems:@[self.composeNoteItem ,self.attendItem] animated:YES];
    }
    else
    {
        self.attendItem.title = @"Attend";
        [self.navigationItem setRightBarButtonItems:@[self.attendItem] animated:YES];
    }
    
    [self.notesButton setTitle:[NSString stringWithFormat:@"Notes count - %lu",(unsigned long)self.eventToPresent.notes.count] forState:UIControlStateNormal];
    
    NSManagedObjectContext* localContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_rootSavingContext]];
    
    [localContext performBlock:^{
        ///finding like of current user
        self.likeOfCurrentUser = [Like MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"(%K == %@) AND (%K == %@)",@"owner",[EVUser currentUser],@"event",self.eventToPresent] inContext:localContext];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            self.likeSwitch.on = self.likeOfCurrentUser != nil;
        }];
        
        ////finding likes of event
        NSInteger count = [Like MR_countOfEntitiesWithPredicate:[NSPredicate predicateWithFormat:@"%K == %@",@"event", self.eventToPresent]];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.likesButton setTitle:[NSString stringWithFormat:@"Total likes %lu",count] forState:UIControlStateNormal];
        }];
    }];
    

    self.startsOnLabel.text = [NSString stringWithFormat:@"Created on %@",event.createdDate.description];
    self.endsOnLabel.text = [NSString stringWithFormat:@"Ends on %@",event.endDate.description];
}

#pragma mark - target/action calls

- (void) switchAttendaceStatus
{
    Event* event = self.eventToPresent;
    
    BOOL isAttendingEvent = [[EVUser currentUser].attendedEvents containsObject:event];
    if(isAttendingEvent)
    {
        [EVUser.currentUser removeAttendedEventsObject:event];
    }
    else
    {
        [EVUser.currentUser addAttendedEventsObject:event];
    }
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
    
    [self refreshView];
}

- (void) presentNoteCompositionUI
{
    UIAlertView* view = [[UIAlertView alloc] initWithTitle:@"Note composition" message:@"Put note text down" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    view.alertViewStyle= UIAlertViewStylePlainTextInput;
    [view show];
}

- (IBAction)showEventOrganizer:(id)sender {
    UserPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[UserPageController restorationId]];
    
    [controller setUserToDisplay:self.eventToPresent.orginizer];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)showParticipants:(id)sender {
    UserListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[UserListController restorationId]];
    
    [controller setFetchedResultsController:[EVUser MR_fetchAllGroupedBy:nil
                                                          withPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS %@",@"attendedEvents",self.eventToPresent]
                                                               sortedBy:@"name"
                                                              ascending:YES]];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)showNotes:(id)sender {
    NotesListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[NotesListController restorationId]];
    
    NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"Note"];
    
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"event",self.eventToPresent];
    [request setRelationshipKeyPathsForPrefetching:@[@"author"]];
    
    controller.fetchedResultsController = [Note MR_fetchController:request
                                                          delegate:nil
                                                      useFileCache:NO
                                                         groupedBy:nil
                                                         inContext:[NSManagedObjectContext MR_defaultContext]];
    [Note MR_performFetch:controller.fetchedResultsController];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)toggleLike:(id)sender {
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        if(self.likeSwitch.isOn)//user has just liked this post
        {
            self.likeOfCurrentUser = [Like MR_createEntityInContext:localContext];
            
            self.likeOfCurrentUser.date = [NSDate date];
            self.likeOfCurrentUser.owner = [[EVUser currentUser] MR_inContext:localContext];
            self.likeOfCurrentUser.event = [self.eventToPresent MR_inContext:localContext];
        }
        else//user has just unliked this post
        {
            [self.likeOfCurrentUser MR_deleteEntityInContext:localContext];
        }
    }];
}

- (IBAction)showLikes:(id)sender {
    LikesController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[LikesController restorationId]];

    NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"Like"];
    
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"event",self.eventToPresent];
    [request setRelationshipKeyPathsForPrefetching:@[@"owner"]];
    
    controller.fetchedResultsController = [Like MR_fetchController:request
                                                          delegate:nil
                                                      useFileCache:NO
                                                         groupedBy:nil
                                                         inContext:[NSManagedObjectContext MR_defaultContext]];
    [Like MR_performFetch:controller.fetchedResultsController];
    
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - alert view delegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)///we need to send a friendRequest
    {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            Note* newNote = [Note MR_createEntityInContext:localContext];
            
            newNote.date = [NSDate date];
            
            newNote.author = (EVUser*)[localContext objectWithID:EVUser.currentUser.objectID];
            newNote.text = [alertView textFieldAtIndex:0].text;
            newNote.event = (Event*)[localContext objectWithID:self.eventToPresent.objectID];
        }];
    }
}

@end
