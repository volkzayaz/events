//
//  ReceivedFriendshipController.h
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "ListProviderController.h"
#import "EVUser.h"

@interface ReceivedFriendshipController : ListProviderController <ListProviderDelegate>

- (void) setUserToDisplay:(EVUser*)user;

@end
