//
//  UserPageController.h
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EVUser.h"

@interface UserPageController : UIViewController

@property (nonatomic, strong) EVUser* userToDisplay;

@end
