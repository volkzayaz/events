//
//  UserListController.m
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "UserListController.h"
#import "UserPageController.h"

#import "UIViewController+RestorationID.h"

#import "EVUser.h"

@interface UserListController ()

@end

@implementation UserListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Users";
}

#pragma mark - list provider delegate methods

- (NSString *)tableViewCellReuseID
{
    return @"cell";
}

- (void) configureCell:(UITableViewCell *)cell withManagedObject:(NSManagedObject *)object
{
    EVUser *user = (EVUser*)object;
    
    cell.textLabel.text = user.basicInfoString;
    cell.detailTextLabel.text = [user.birthDate description];
    cell.imageView.image = [UIImage imageWithData:user.avatar];
}

- (void)didSelectManagedObject:(NSManagedObject *)object
{
    UserPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[UserPageController restorationId]];
    
    [controller setUserToDisplay:(EVUser*)object];
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end
