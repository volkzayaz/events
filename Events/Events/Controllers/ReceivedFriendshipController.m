//
//  ReceivedFriendshipController.m
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "ReceivedFriendshipController.h"
#import "FriendRequestCell.h"

#import "UIViewController+ErrorMessages.h"

#import <MagicalRecord/MagicalRecord.h>

@interface ReceivedFriendshipController ()

@property (nonatomic, strong) EVUser* friendRequestsReceiver;

@end

@implementation ReceivedFriendshipController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 50.;
}

- (void) setUserToDisplay:(EVUser*)user
{
    self.friendRequestsReceiver = user;
    
    self.fetchedResultsController = [EVUser MR_fetchAllGroupedBy:nil
                                                   withPredicate:[NSPredicate predicateWithFormat:@"%@ IN %K",user,@"invitationSentFriends"]
                                                        sortedBy:@"name"
                                                       ascending:YES];
}

#pragma mark - List Provider Delegate

- (NSString*) tableViewCellReuseID
{
    return NSStringFromClass([FriendRequestCell class]);
}

- (void)configureCell:(FriendRequestCell *)cell withManagedObject:(EVUser *)object
{
    EVUser* userThatSentFriendRequest = object;
    
    __weak typeof(self) weakSelf = self;
    [cell setUser:userThatSentFriendRequest withCallback:^() {
        ///we intentionally allow friend request acceptance by any user
        ///so it's ok for current user to be John, while he accepts friend request form Paul to Kate.
        
        [weakSelf.friendRequestsReceiver removeInvitationReceivedFriendsObject:userThatSentFriendRequest];
        [weakSelf.friendRequestsReceiver addFriendsObject:userThatSentFriendRequest];
        
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
        
        [weakSelf showInfoMessage:[NSString stringWithFormat:@"You are now friends with %@", userThatSentFriendRequest.name] withTitle:@"Success"];
    }];
    cell.addFriendButton.hidden = NO;
}

@end
