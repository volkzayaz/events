//
//  LikesController.m
//  Events
//
//  Created by Vladislav Soroka on 7/6/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "LikesController.h"
#import "UserPageController.h"

#import "UIViewController+RestorationID.h"

#import "Like.h"
#import "EVUser.h"


@interface LikesController () <ListProviderDelegate>

@end

@implementation LikesController

- (NSString*) tableViewCellReuseID
{
    return @"cell";
}

- (void)configureCell:(UITableViewCell *)cell withManagedObject:(Like *)object
{
    cell.textLabel.text = [object.owner basicInfoString];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Like date %@",object.date.description];
    cell.imageView.image = [UIImage imageWithData:object.owner.avatar];
}

- (void) didSelectManagedObject:(Like *)object
{
    UserPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:[UserPageController restorationId]];
    
    [controller setUserToDisplay:object.owner];
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end
