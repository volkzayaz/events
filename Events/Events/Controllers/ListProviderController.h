//
//  ListProviderController.h
//  Events
//
//  Created by Vladislav Soroka on 7/3/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreData;

@protocol ListProviderDelegate <NSObject>
@required
- (NSString*) tableViewCellReuseID;
- (void) configureCell:(UITableViewCell*)cell withManagedObject:(NSManagedObject*)object;

@optional
- (void) didSelectManagedObject:(NSManagedObject*)object;

@end

/**
 *  @discussion - ListProviderController is a base class for representing of potentially big amount of data within single entity. Class requires properly configured fetchedResultsController. It allows responding to entity selection events and customization of presentation cells via it's delegate.
 */
@interface ListProviderController : UITableViewController

@property (nonatomic, copy) NSFetchedResultsController* fetchedResultsController;
@property (nonatomic, weak) id<ListProviderDelegate> listProviderDelegate;

@end
