//
//  AppDelegate.m
//  Events
//
//  Created by Vladislav Soroka on 7/1/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "AppDelegate.h"
#import <MagicalRecord/MagicalRecord.h>
#import "UserListController.h"
#import "UIViewController+RestorationID.h"
#import "EVUser+CurrentUser.h"
#import "Event.h"

#import "UserCreationController.h"
#import "EventsListController.h"

#import "DiffManager.h"
#import "UpdatesManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [MagicalRecord setupCoreDataStack];
    [[DiffManager sharedInstance] startListeningForChanges];
    
    
    UITabBarController* tabBarViewController = (UITabBarController*)self.window.rootViewController;
    
    ///user management tab
    UserCreationController* userManagement = [tabBarViewController.storyboard instantiateViewControllerWithIdentifier:[UserCreationController restorationId]];
    UINavigationController* userManagementNavigation = [[UINavigationController alloc] initWithRootViewController:userManagement];
    userManagement.title = @"Management";
    
    
    ///events timeline tab
    EventsListController* eventsTimeline = [tabBarViewController.storyboard instantiateViewControllerWithIdentifier:[EventsListController restorationId]];
    [eventsTimeline turnTimelineModeOn];
    UINavigationController* eventsTimelineNavigation = [[UINavigationController alloc] initWithRootViewController:eventsTimeline];
    eventsTimeline.title = @"Timeline";
    
    
    ///userlist tab
    UserListController* userList = [tabBarViewController.storyboard instantiateViewControllerWithIdentifier:[UserListController restorationId]];
    userList.fetchedResultsController = [EVUser MR_fetchAllGroupedBy:nil
                                                       withPredicate:nil
                                                            sortedBy:@"name"
                                                           ascending:YES];
    UINavigationController* controller = [[UINavigationController alloc] initWithRootViewController:userList];
    controller.tabBarItem.title = @"Users";
    
    
    [tabBarViewController setViewControllers:@[userManagementNavigation,eventsTimelineNavigation,controller]];
    
    return YES;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(event.type == UIEventSubtypeMotionShake)
    {
        [[UpdatesManager sharedInstance] startUpdateSimulation];
    }
}

@end
