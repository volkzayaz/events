//
//  UIViewController+RestorationID.h
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (RestorationID)

+ (NSString*) restorationId;

@end
