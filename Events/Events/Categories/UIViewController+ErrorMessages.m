//
//  UIViewController+ErrorMessages.m
//  Rumbble
//
//  Created by Vlad Soroka on 07.04.15.
//  Copyright (c) 2015 Rumbble Inc. All rights reserved.
//

#import "UIViewController+ErrorMessages.h"

@implementation UIViewController (ErrorMessages)

- (void) showErrorMessage:(NSString *)message
{
    [self showInfoMessage:message withTitle:NSLocalizedString(@"Error", nil)];
}

- (void) presentError:(NSError *)error
{
    NSString* message = [error localizedDescription];
    
    [self showErrorMessage:message];
}

- (void) showInfoMessage:(NSString *)text withTitle:(NSString *)title
{
    [self showInfoMessage:text withTitle:title callback:nil];
}

- (void) showInfoMessage:(NSString *)text withTitle:(NSString *)title callback:(MessageCallback)callback
{
    UIAlertController* controler = [UIAlertController alertControllerWithTitle:title message:text
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    [controler addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if(callback)
            callback();
    }]];
    
    [controler addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:controler animated:YES completion:nil];
}


@end
