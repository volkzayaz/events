//
//  NSDate+YearsOld.h
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (YearsOld)

- (NSInteger) yearsOld;

- (NSInteger) hoursOld;

@end
