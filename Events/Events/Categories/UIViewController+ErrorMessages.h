//
//  UIViewController+ErrorMessages.h
//  Rumbble
//
//  Created by Vlad Soroka on 07.04.15.
//  Copyright (c) 2015 Rumbble Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^MessageCallback)();

@interface UIViewController (ErrorMessages)

- (void) showErrorMessage:(NSString*)message;

- (void) presentError:(NSError*)error;

- (void) showInfoMessage:(NSString*)text withTitle:(NSString*)title;

- (void) showInfoMessage:(NSString*)text withTitle:(NSString*)title callback:(MessageCallback)callback;


@end
