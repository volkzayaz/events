//
//  NSDate+YearsOld.m
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "NSDate+YearsOld.h"

@implementation NSDate (YearsOld)

- (NSInteger)yearsOld
{
    NSDate *earlier = self;
    NSDate *today = [NSDate date];
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    // pass as many or as little units as you like here, separated by pipes
    NSUInteger units = NSCalendarUnitYear;
    
    NSDateComponents *components = [gregorian components:units fromDate:earlier toDate:today options:0];

    return [components year];
}

- (NSInteger) hoursOld
{
    NSDate *earlier = self;
    NSDate *today = [NSDate date];
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    // pass as many or as little units as you like here, separated by pipes
    NSUInteger units = NSCalendarUnitHour;
    
    NSDateComponents *components = [gregorian components:units fromDate:earlier toDate:today options:0];
    
    return [components hour];
}

@end
