//
//  UIViewController+RestorationID.m
//  Events
//
//  Created by Vladislav Soroka on 7/2/15.
//  Copyright (c) 2015 Vladislav Soroka. All rights reserved.
//

#import "UIViewController+RestorationID.h"

@implementation UIViewController (RestorationID)

+ (NSString*) restorationId
{
    return NSStringFromClass([self class]);
}

@end
